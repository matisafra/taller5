import numpy as np
import time
import random
import sys

global N


def matrizCeros():
    matriz =[]
    for i in range(N):
        matriz.append([0.0]*N)
    return matriz

def matrizAleatoria(a):
    for n in range(len(a)):
        for j in range(len(a[n])):
            a[n][j] = random.random()
    return a
            

def multMatrices(a, b, res):
    if isinstance(a, np.matrix):
        mult = np.matmul(a,b) 
        np.copyto(res,mult) 
    else:     
        for i in range(0,N): # i filas
            for j in range(0,N): # j columnas
                for k in range(0,N): # k contador
                    res[i][j] += a[i][k]*b[k][j] # matriz resultado
    return res

def medirTiempos(fn, *args):
    inicio = time.time()
    print(fn(*args))
    final = time.time()
    tiempo = float(final - inicio) 
    return tiempo

def realizarExperimento():
    A = matrizCeros()
    B = matrizCeros()
    C = matrizCeros() #tres matrices 0 de N x N
    
    matrizAleatoria(A)
    matrizAleatoria(B)
    
    medirTiempos(multMatrices, A, B, C)
    print('Tiempo total listas: ', medirTiempos(multMatrices, A, B, C) )
    
    A = np.matrix(matrizAleatoria(A))
    B = np.matrix(matrizAleatoria(B))
    C = np.matrix(matrizCeros()) #redefino A, B y C para evaluar numpy
    
    medirTiempos(multMatrices, A, B, C)
    print('Tiempo total numpy: ', medirTiempos(multMatrices, A, B, C))

realizarExperimento() 
 
    

if __name__ == '__main__':
    if len(sys.argv) > 1:
        N = int(sys.argv[1])
    realizarExperimento()
